#!/bin/bash

# add mac settings:
echo "Auto hide dock without delay"
defaults write com.apple.dock autohide -bool true && \
	defaults write com.apple.dock autohide-delay -float 0 && \
	defaults write com.apple.dock autohide-time-modifier -float 0 && \
	defaults write com.apple.dock largesize -int 38 && \
	defaults write com.apple.dock magnification -bool true && \
	defaults write com.apple.dock tilesize -int 27 && \
	killall Dock

echo "Set login screen message"
sudo defaults write /Library/Preferences/com.apple.loginwindow LoginwindowText 'Ahoj .. Hi .. Cześć\n\nHave a nice day!'

echo "Open tabs not windows"
defaults write NSGlobalDomain AppleWindowTabbingMode -string 'always'

echo "fast "
defaults write NSGlobalDomain com.apple.trackpad.scaling -float 2.5

echo "Show hidden files in Finder"
defaults write com.apple.finder AppleShowAllFiles -bool true

echo "Clean all the icons in dock"
defaults write com.apple.dock persistent-apps -array

echo "Disable screensaver"
defaults -currentHost write com.apple.screensaver idleTime -int 0

echo "Show dashboard"
defaults write com.apple.dashboard enabled-state -int 1
defaults write com.apple.dock dashboard-in-overlay -bool true && killall Dock

echo "Show digital time"
defaults write com.apple.menuextra.clock IsAnalog -bool false
defaults write com.apple.menuextra.battery -bool true

echo "Disable the 'Are you sure you want to open this application?' dialog"
defaults write com.apple.LaunchServices LSQuarantine -bool false

echo "Allow applications downloaded from anywhere"
spctl --master-disable

echo "Disable hard disk sleep"
sudo pmset -a disksleep 0

echo "Disable hibernation (speeds up entering sleep mode)"
sudo pmset -a hibernatemode 0

#echo "Disable press-and-hold for keys in favor of key repeat"
#defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

echo "Show desktop when mouse hit to right top corner"
defaults write com.apple.dock wvous-tr-corner   -int 4

echo "Ask for password without delay"
defaults write com.apple.screensaver askForPassword -bool true
defaults write com.apple.screensaver askForPasswordDelay -int 0

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# clean homebrew
brew cleanup
brew cask cleanup
brew update

# install brew dependencies
brew install zsh zsh-completions
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
brew install curl
brew install go
brew install openssl
brew install phantomjs
brew install wget
brew install sqlite
brew install zsh
brew install kubernetes-cli
brew install node6-lts
brew install yarn
brew install homebrew/php/php70
brew install homebrew/php/composer

# install cask
brew tap caskroom/cask
brew tap caskroom/versions

# install cask dependencies
brew cask install airflow
brew cask install alfred
brew cask install vlc
brew cask install 1password
brew cask install google-cloud-sdk
brew cask install intellij-idea
brew cask install iterm2
brew cask install skype
brew cask install slack
brew cask install spotify
brew cask install sublime-text
brew cask install firefox
brew cask install google-chrome
brew cask install google-chrome-canary
brew cask install spectacle
brew cask install vagrant
brew cask install virtualbox
brew cask install tunnelblick
brew cask install visual-studio-code
brew cask install webstorm
brew cask install whatsapp
brew cask install silverlight
brew cask install docker

echo "Change iTerm to be scrollable in vim etc"
defaults write com.googlecode.iterm2 AlternateMouseScroll -bool true

