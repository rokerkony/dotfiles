# Installation notes

run:

```
git clone git@bitbucket.org:rokerkony/dotfiles.git /tmp/dotfiles-tmp
mv /tmp/dotfiles-tmp/.git $HOME/.git
rm -rf /tmp/dotfiles-tmp`

cd $HOME/install

install.sh
git checkout ./.zshrc
```
