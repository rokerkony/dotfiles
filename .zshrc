export ZSH_DISABLE_COMPFIX=true
export PATH="/usr/local/sbin:$HOME/bin:$PATH"
# export PATH="/usr/local/opt/python/libexec/bin:$PATH"

# conda installed over brew cask:
export PATH="/usr/local/miniconda2/bin:$PATH"

fpath=(/usr/local/share/zsh-completions $fpath)

# Path to your oh-my-zsh installation.
export ZSH=/Users/konradcerny/.oh-my-zsh

# homebrew token
export HOMEBREW_GITHUB_API_TOKEN="df9d3e9169ea388f4d1c6a65d475b5e084b299e6"

# add golang installed by brew
export PATH=$PATH:/usr/local/opt/go/libexec/bin

# add java to terminal
export PATH=/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin:$PATH
# java_home is failing for gradle
#export JAVA_HOME='/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java'
#export JAVA_HOME=/usr/
export GROOVY_HOME=/usr/local/opt/groovy/libexec

# switch PHP version
export PATH="/usr/local/opt/php@8.0/bin:$PATH"

ZSH_THEME="robbyrussell"

plugins=(cp docker docker-compose git gradle node npm osx terraform yarn)

source $ZSH/oh-my-zsh.sh

PATH_TO_WIKI_REPO="${HOME}/Sites/erento/wiki"
source "${PATH_TO_WIKI_REPO}/dotfiles/.erentorc"

. ~/dotfiles/config
. ~/dotfiles/aliases

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . /usr/local/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/konradcerny/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/konradcerny/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/konradcerny/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/konradcerny/google-cloud-sdk/completion.zsh.inc'; fi

